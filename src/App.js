import React, { Component } from "react";
import ListeMessages from "./components/ListeMessages";
import ListeSalles from "./components/ListeSalles";
import NouvelleSalle from "./components/NouvelleSalle";
import Login from "./components/Login";
import FormulaireEnvoiMessages from "./components/FormulaireEnvoiMessages";
import Chatkit from "@pusher/chatkit-client";
import Routes from "./Routes/Routes";
import { Redirect } from "react-router-dom";

import { tokenUrl, instanceLocator } from "./config";

class App extends Component {
  constructor() {
    super();
    this.state = {
      messages: [],
      joinableRooms: [],
      joinedRooms: [],
      salleID: null,
      userId: ""
    };
  }

  instanceApp = () => {
    const myChatManager = new Chatkit.ChatManager({
      instanceLocator,
      userId: this.state.userId,
      tokenProvider: new Chatkit.TokenProvider({ url: tokenUrl })
    });
    myChatManager
      .connect()
      .then(currentUser => {
        this.currentUser = currentUser;
        this.getRooms();
      })
      .catch(err => console.log("erreur lors de la connexion", err));
  };
  componentDidMount() {
    if (this.state.userId != null) {
      this.instanceApp();
    }
  }

  setUserID = userID => {
    this.setState({ userId: userID });
    this.instanceApp();
  };

  subscribeToRoom = salleID => {
    this.setState({ messages: [] });
    this.currentUser
      .subscribeToRoom({
        roomId: salleID,
        hooks: {
          onMessage: message => {
            this.setState({
              messages: [...this.state.messages, message]
            });
          }
        }
      })
      .then(room => {
        this.setState({ salleID: room.id });
        this.getRooms();
      })
      .catch(err => console.log("erreur lors de l'entrée dans la salle ", err));
  };
  getRooms = () => {
    this.currentUser
      .getJoinableRooms()
      .then(joinableRooms => {
        this.setState({
          joinableRooms,
          joinedRooms: this.currentUser.rooms
        });
      })
      .catch(err =>
        console.log(
          "erreur lors de la récuperation des salles disponibles",
          err
        )
      );
  };

  sendMessage = text => {
    this.currentUser.sendMessage({
      text,
      roomId: this.state.salleID
    });
  };

  createRoom = nomSalle => {
    this.currentUser
      .createRoom({
        name: nomSalle
      })
      .then(room => this.subscribeToRoom(room.id))
      .catch(err => console.log("erreur lors de la création de la salle"));
  };

  render() {
    return (
      <div className="App">
        <ListeSalles
          salleID={this.state.salleID}
          rejoindreSalle={this.subscribeToRoom}
          salles={[...this.state.joinableRooms, ...this.state.joinedRooms]}
        />
        <ListeMessages
          messages={this.state.messages}
          salleID={this.state.salleID}
        />
        <FormulaireEnvoiMessages
          disabled={!this.state.salleID}
          sendMessage={this.sendMessage}
        />
        <NouvelleSalle createRoom={this.createRoom} />
        <div className="Login">
          <div
            className="needLogin"
            style={{ display: this.state.userId === "" ? "block" : "none" }}
          >
            <Login setUserID={this.setUserID} />
          </div>
          <div
            className="Logged"
            style={{ display: this.state.userId === "" ? "none" : "block" }}
          >
            <h3> Vous etes connectés en tant que : {this.state.userId}</h3>
          </div>
        </div>
        <Routes />
      </div>
    );
  }
}

export default App;
