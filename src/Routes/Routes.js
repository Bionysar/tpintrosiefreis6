import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "../components/Login.js";
import Chat from "../App.js";
{
  /*import NotFound from "../pages/NotFound";*/
}

export default () => (
  <Switch>
    <Route path="/Login" exact component={Login} />
    <Route path="/Chat" exact component={Chat} />
    {/* catch all unmatched routes and redirect them to 404 not found page}
    <Route component={NotFound} />*/}
  </Switch>
);
