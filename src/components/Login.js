import React, { Component } from "react";
import ReactDOM from "react-dom";
import Message from "./Message";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.css";
import { Nav } from "react-bootstrap";
import Routes from "../Routes/Routes";
import { LinkContainer } from "react-router-bootstrap";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      apiResponse: "undefineapi"
    };
  }

  componentWillMount() {
    console.log("First this called");
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  changeFormValue = () => {
    if (this.state.apiResponse === "Connexion réussi") {
      this.props.setUserID(
        this.state.email.substr(0, this.state.email.indexOf("@"))
      );
    } else {
      console.log("NON");
    }
  };
  handleSubmit = event => {
    event.preventDefault();
    fetch("http://localhost:9000/login", {
      method: "POST",
      body: JSON.stringify({
        Data: { login: this.state.email, pass: this.state.password }
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.text())
      .then(res => this.setState({ apiResponse: res }))
      .then(res => this.changeFormValue())
      .catch(error => console.error("Error:", error));
  };

  render() {
    return (
      <div className="Login">
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </Form.Group>
          <Button
            block
            variant="primary"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
        </Form>
      </div>
    );
  }
}
