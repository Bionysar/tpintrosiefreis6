import React, { Component } from "react";

class FormulaireEnvoiMessages extends Component {
  constructor() {
    super();
    this.state = {
      message: ""
    };
  }

  handleChanges = e => {
    this.setState({
      message: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.sendMessage(this.state.message);
    this.setState({
      message: ""
    });
  };

  render() {
    return (
      <form className="FormulaireEnvoiMessages" onSubmit={this.handleSubmit}>
        <input
          disabled={this.props.disabled}
          onChange={this.handleChanges}
          value={this.state.message}
          placeholder="Ecrire votre message ici"
          type="text"
          onFocus={e => (e.target.placeholder = "")}
          onBlur={e => (e.target.placeholder = "Ecrire votre message ici")}
        />
      </form>
    );
  }
}

export default FormulaireEnvoiMessages;
