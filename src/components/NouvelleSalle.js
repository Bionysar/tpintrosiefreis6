import React, { Component } from "react";

class NouvelleSalle extends Component {
  constructor() {
    super();
    this.state = {
      nomSalle: ""
    };
  }
  handleChange = e => {
    this.setState({
      nomSalle: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.createRoom(this.state.nomSalle);
    this.setState({
      nomSalle: ""
    });
  };

  render() {
    return (
      <div className="NouvelleSalle">
        <form onSubmit={this.handleSubmit}>
          <input
            onChange={this.handleChange}
            value={this.state.nomSalle}
            type="text"
            placeholder="Creer une nouvelle salle"
            onFocus={e => (e.target.placeholder = "")}
            onBlur={e => (e.target.placeholder = "Creer une nouvelle salle")}
            required
          />
          <button id="NouvelleSalle-btn" />
        </form>
      </div>
    );
  }
}

export default NouvelleSalle;
