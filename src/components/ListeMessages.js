import React, { Component } from "react";
import ReactDOM from "react-dom";
import Message from "./Message";

class ListeMessages extends Component {
  componentWillUpdate() {
    const node = ReactDOM.findDOMNode(this);
    this.shouldScrollToBottom =
      node.scrollTop + node.clientHeight + 100 >= node.scrollHeight;
  }

  componentDidUpdate() {
    if (this.shouldScrollToBottom) {
      const node = ReactDOM.findDOMNode(this);
      node.scrollTop = node.scrollHeight;
    }
  }

  render() {
    if (!this.props.salleID) {
      return (
        <div className="ListeMessages">
          <div className="rejoindreSalle">&larr; Rejoignez une salle !</div>
        </div>
      );
    }
    return (
      <div className="ListeMessages">
        {this.props.messages.map((message, index) => {
          return (
            <Message
              key={index}
              username={message.senderId}
              text={message.text}
            />
          );
        })}
      </div>
    );
  }
}

export default ListeMessages;
