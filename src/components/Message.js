import React from "react";

function Message(props) {
  return (
    <div className="Messages">
      <div className="Messages-nomUtilisateur">{props.username}</div>
      <div className="Messages-texte">{props.text}</div>
      <hr />
    </div>
  );
}

export default Message;
