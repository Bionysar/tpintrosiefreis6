import React, { Component } from "react";

class ListeSalles extends Component {
  render() {
    const sallesTries = [...this.props.salles].sort((a, b) => a.id - b.id);
    return (
      <div className="ListeSalles">
        <ul>
          {sallesTries.map(salle => {
            const active = this.props.salleID === salle.id ? "active" : "";
            return (
              <li key={salle.id} className={"Salle " + active}>
                <a onClick={() => this.props.rejoindreSalle(salle.id)} href="#">
                  # {salle.name}
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default ListeSalles;
