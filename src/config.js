const tokenUrl =
  "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/527a9be5-f484-4c94-a25c-4795e15be4c7/token";

const instanceLocator = "v1:us1:527a9be5-f484-4c94-a25c-4795e15be4c7";

export { tokenUrl, instanceLocator };
